package cjz.gdjproject2.main;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import org.json.JSONException;
import org.json.JSONObject;


public class ProtocolServer {
	private static BufferedOutputStream bufferedOutputStream = null;
	private static BufferedOutputStream bufferedOutputStreamToCameraService = null;
	private static class Protocol{
		/**协议b：有人进来**/
		public final static byte[] cameraOn =    new byte[]{(byte)0xff, (byte)0xff, (byte)0x01, (byte)0xfc, (byte)0xfc};
		public final static byte[] heartBeat =        new byte[]{(byte)0xff, (byte)0xff, (byte)0x00, (byte)0xfc, (byte)0xfc};
		public final static byte[] securityEnable =   new byte[]{(byte)0xff, (byte)0x0a, (byte)0x01, (byte)0xfc, (byte)0xfc};
		public final static byte[] securityDisable =  new byte[]{(byte)0xff, (byte)0x0a, (byte)0x00, (byte)0xfc, (byte)0xfc};
		public static class StatusFlag{
			public static boolean securityOn = true;
			public static boolean cameraIsOn = true;
		}
	}
	public static void main(String args[]){
		System.out.println("启动协议服务");
		initController.start();
		new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("init client know now status ....");
				System.out.println("初始化客户端链接");
				initClientKnowNowStatus();
			}
		}).start();
		initProccessor();

	}
	
	private static void initProccessor(){
		try {
			ServerSocket serverSocket = new ServerSocket(8899);
			ServerSocket serverSocketToCameraService = new ServerSocket(20014);
			while(true){
				System.out.println("等待连接");
				final Socket socketToDevice = serverSocket.accept();
				final Socket socketToCameraService = serverSocketToCameraService.accept();
				System.out.println("连接完成");
				new Thread(new Runnable() {
					public void run() {
						BufferedInputStream bufferedInputStream = null;
						BufferedOutputStream bufferedOutputStream = null;
						BufferedOutputStream bufferedOutputStreamToCameraService = null;
						try {
							bufferedInputStream = new BufferedInputStream(socketToDevice.getInputStream());
							bufferedOutputStream = new BufferedOutputStream(socketToDevice.getOutputStream());
							bufferedOutputStreamToCameraService = new BufferedOutputStream(socketToCameraService.getOutputStream());
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						//对门禁设备输出设防或不设防的信号
						setSafeOrNotSafeMode(bufferedOutputStream);
						//获取门禁的协议输入信号，循环阻塞，做协议分析
						loop(bufferedInputStream, bufferedOutputStreamToCameraService);
						try {
							socketToDevice.close();
							socketToCameraService.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}).start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void initProccessor2(){
		try {
			ServerSocket serverSocket = new ServerSocket(8899);
			ServerSocket serverSocketToCameraService = new ServerSocket(20014);
			while(true){
				System.out.println("等待连接");
				final Socket socketToDevice = serverSocket.accept();
				final Socket socketToCameraService = serverSocketToCameraService.accept();
				System.out.println("连接完成");
				new Thread(new Runnable() {
					public void run() {
						try {
							bufferedOutputStream = new BufferedOutputStream(socketToDevice.getOutputStream());
							bufferedOutputStreamToCameraService = new BufferedOutputStream(socketToCameraService.getOutputStream());
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						/**客户端连接线程**/
						new Thread(new Runnable() {
							@Override
							public void run() {
								ServerSocket serverSocketForPhoneClient;
								try {
									System.out.println("run socket........");
									serverSocketForPhoneClient = new ServerSocket(8900);
									while(true){
										Socket socketForPhoneClient = serverSocketForPhoneClient.accept();
										/**循环阻塞，做协议分析**/
										loop(new BufferedInputStream(socketForPhoneClient.getInputStream()), bufferedOutputStreamToCameraService);
									}
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
						}).start();
						/**循环阻塞**/
						setSafeOrNotSafeMode2(bufferedOutputStream);
						try {
							socketToDevice.close();
							socketToCameraService.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}).start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**做协议分析**/
	private static void loop(BufferedInputStream bufferedInputStream, BufferedOutputStream bufferedOutputStreamToCameraService){
		try {
			System.out.println("loop..........;");
			while(true){
				byte data[] = new byte[10];
				while(bufferedInputStream.read(data) != -1){
					System.out.println("read date ........");
					analyzeProtocolData(data, bufferedOutputStreamToCameraService);
				}
			}
		} catch (Exception e) {
			System.out.println("检测线程遇到断开情况");
		}
	}
	
	/**协议判断**/
	private static void analyzeProtocolData(byte[] data, BufferedOutputStream bufferedOutputStreamToCameraService){
		if(data[1] == Protocol.cameraOn[1] && data[2] == Protocol.cameraOn[2]){
			sendSomeoneComeing(bufferedOutputStreamToCameraService);
			
		}
		else if(data[1] == Protocol.heartBeat[1] && data[2] == Protocol.heartBeat[2]){
			System.out.println("心跳包");
		}
	}
	
//	private static void getSaveOrNotSafeMode(finale inputsteam) {
//		....
//	}
	
	
	/**设防或不设防**/
	private static void setSafeOrNotSafeMode(final BufferedOutputStream bufferedOutputStream){
		new Thread(new Runnable() {
			public void run() {
				try {
					while(true){
						//不设防的时候不用录像
						if(!Protocol.StatusFlag.securityOn){
							Protocol.StatusFlag.cameraIsOn = false;
							bufferedOutputStream.write(Protocol.securityDisable);
							bufferedOutputStream.flush();
							System.out.println("不设防");
						}
						if(Protocol.StatusFlag.securityOn){
							Protocol.StatusFlag.cameraIsOn = true;
							bufferedOutputStream.write(Protocol.securityEnable);
							bufferedOutputStream.flush();
							System.out.println("设防");
						}
						Thread.sleep(5000);
					}
				} catch (IOException e) {
					System.out.println("设防失败，可能是硬件设备断开了连接");
//					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
	
	/**设防或不设防**/
	private static void setSafeOrNotSafeMode2(final BufferedOutputStream bufferedOutputStream){
		try {
			while(true){
				//不设防的时候不用录像
				if(!Protocol.StatusFlag.securityOn){
					Protocol.StatusFlag.cameraIsOn = false;
					bufferedOutputStream.write(Protocol.securityDisable);
					bufferedOutputStream.flush();
					System.out.println("不设防");
				}
				if(Protocol.StatusFlag.securityOn){
					Protocol.StatusFlag.cameraIsOn = true;
					bufferedOutputStream.write(Protocol.securityEnable);
					bufferedOutputStream.flush();
					System.out.println("设防");
				}
				Thread.sleep(5000);
			}
		} catch (IOException e) {
			System.out.println("设防失败，可能是硬件设备断开了连接");
//			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**发送有人进来指令**/
	private static void sendSomeoneComeing(BufferedOutputStream bufferedOutputStream){
		try {
			if(Protocol.StatusFlag.cameraIsOn){
				bufferedOutputStream.write(Protocol.cameraOn);
				bufferedOutputStream.flush();
				System.out.println("发送有人进来指令");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**手机端控制协议端flag**/
	private static Thread initController = new Thread(new Runnable() {
		class Cmd{
			private static final String CMD_SetSecurityOn = "SetSecurityOn";
			private static final String CMD_SetSecurityOff = "SetSecurityOff";
		}
		
		public void run() {
			System.out.println("init controller start");
			while(true){
				System.out.println("client socket....");
				ServerSocket serverSocketCtrl = initServerSocket();
				System.out.println("heyheyhey.........." );
				while(true){
					System.out.println("gggggg");
					final Socket socketClient = initSocket(serverSocketCtrl);
					System.out.println("99999999999");
					new Thread(new Runnable() {
						public void run() {
							analyze(socketClient);
						}
					}).start();
				}
			}
		}
		
		private ServerSocket initServerSocket(){
			try {
				return new ServerSocket(20015);
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}	
		}
		
		private Socket initSocket(ServerSocket serverSocketClientContrller){
			try {
				System.out.println("010101010101");
				Socket socketClientContrller = serverSocketClientContrller.accept();
				System.out.println("12121212");
				return socketClientContrller;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		}
		
		private void analyze(Socket socketClient){
			try {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socketClient.getInputStream()));
				String tempCmd = null;
				while((tempCmd = bufferedReader.readLine())  != null){
					switch(tempCmd){
						case Cmd.CMD_SetSecurityOn:
							System.out.println("收到手机客户端设防命令!!!!!!!!!!!");
							ProtocolServer.Protocol.StatusFlag.securityOn = true;
							break;
						case Cmd.CMD_SetSecurityOff:
							System.out.println("收到手机客户端解除设防命令$$$$$$$$$$");
							ProtocolServer.Protocol.StatusFlag.securityOn = false;
							break;
					}
				}
			} catch (IOException e) {
				//e.printStackTrace();
				System.out.println("手机客户端已断开");
			}
		}
	});
	
	/**让客户端知道现在的情况**/
	private static void initClientKnowNowStatus(){
		ServerSocket serverSocket;
		try {
			serverSocket = new ServerSocket(20020);
			while(true){
				System.out.println("等待手机连接以获取当前是否设防等状态");
				final Socket socketPhone = serverSocket.accept();
				System.out.println("know now status set....");
				new Thread(new Runnable() {
					@Override
					public void run() {

						BufferedOutputStream bufferedOutputStream = null;
						try {
							bufferedOutputStream = new BufferedOutputStream(socketPhone.getOutputStream());
						} catch (IOException e1) {e1.printStackTrace();}
						try {
							while(!socketPhone.isClosed() && socketPhone.isConnected()){
								System.out.println("socket phone not closed !!!");
								JSONObject jsonObject = new JSONObject();
								jsonObject.put("securityOn", Protocol.StatusFlag.securityOn);
								jsonObject.put("cameraIsOn", Protocol.StatusFlag.cameraIsOn);
								bufferedOutputStream.write((jsonObject.toString() + "\n").getBytes());
								bufferedOutputStream.flush();
								Thread.sleep(1000);
								System.out.println("11111111111");
								socketPhone.close();
							} 
						}
						catch (IOException e) {
							e.printStackTrace();
							} catch (InterruptedException e) {
								e.printStackTrace();
							} catch (JSONException e) {
								e.printStackTrace();
							}
						
					}
				}).start();
			}
		} catch (IOException e) {
			System.out.println("端口冲突");
		}
		
	}
}
