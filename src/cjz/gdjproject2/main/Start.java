package cjz.gdjproject2.main;

public class Start {
	public static void main(String args[]){
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				ProtocolServer protocolServer = new ProtocolServer();
				protocolServer.main(null);
			}
		}).start();
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				ServerMain main = new ServerMain();
				main.main(null);
			}
		}).start();
	}
}
