package cjz.gdjproject2.main;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ServerMain {
	private static int data = 0;
	private static byte[] data_arr = new byte[1024];
	private static ServerSocket serverSocket;
	private static FileOutputStream fileOutputStream;
	private static String rootPath = "E:\\video\\";

	public static void main(String args[]) {
		System.out.println("init");
		try {
			serverSocket = new ServerSocket(20013);
		} catch (IOException e1) {
			//e1.printStackTrace();
		}
		while (true) {
			try {
				final Socket socket = serverSocket.accept();
				//可以多次连接进来，多线程多文件保存：
				new Thread(new Runnable() {
					public void run() {
						try {
							record(socket, new SimpleDateFormat("yyyy年MM月dd日_HH时mm分ss秒").format(new Date()).toString());
						} catch (IOException e) {
							//e.printStackTrace();
						}
					}
				}).start();
			} catch (Exception e) {
				e.printStackTrace();
				if(fileOutputStream != null)
					try {
						fileOutputStream.close();
					} catch (IOException e1) {
						//e1.printStackTrace();
					}
			}
		}
	}
	
	/**传入新的视频数据**/
	private static void record(Socket socket, String fileName) throws IOException{
		BufferedInputStream bufferedInputStream = new BufferedInputStream(socket.getInputStream());
		File fileDir = new File(rootPath);
		if(!fileDir.exists()){
			fileDir.mkdir();
		}
		File file = new File(rootPath + fileName + ".mp4");
		if(!file.exists()){
			file.createNewFile();
		}
		System.out.println("正在写入：" + fileName);
		fileOutputStream = new FileOutputStream(file);
		while ((/*data = */bufferedInputStream.read(data_arr)) != -1) {
			//System.out.println(data);
			fileOutputStream.write(data_arr);
			fileOutputStream.flush();
		}
		System.out.println("文件：" + fileName + "写入完成");
		fileOutputStream.close();
	}
}
