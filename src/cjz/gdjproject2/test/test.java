package cjz.gdjproject2.test;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class test {
	class Cmd{
		private static final String CMD_SetSecurityOn = "SetSecurityOn";
		private static final String CMD_SetSecurityOff = "SetSecurityOff";
	}
	
	public static void main(String args[]){
		try {
			Socket socket = new Socket("127.0.0.1", 20015);
			BufferedOutputStream bufferedOutputStream  = new BufferedOutputStream(socket.getOutputStream());
			for(int i=0; i<10; i++){
				bufferedOutputStream.write((Cmd.CMD_SetSecurityOn + "\n").getBytes());
				bufferedOutputStream.flush();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
