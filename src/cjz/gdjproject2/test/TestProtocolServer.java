package cjz.gdjproject2.test;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TestProtocolServer {
	private static ServerSocket serverSocket;
	private static class Protocol{
		/**协议：有人进来**/
		public final static byte[] someoneComing = new byte[]{(byte)0xff, (byte)0xff, (byte)0x01, (byte)0xfc, (byte)0xfc};
	}
	
	public static void main(String args[]){
		while(true){
			try {
				serverSocket = new ServerSocket(20014);
			} catch (IOException e1) {
				//e1.printStackTrace();
			}
			while (true) {
				try {
					final Socket socket = serverSocket.accept();
					final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(socket.getOutputStream());
					System.out.println("协议服务端接受到一个视频录制客户端连接请求");
					new Thread(new Runnable() {
						public void run() {
							try {
								for(int i=0; i<30; i++){
									Thread.sleep(100);
									try {
										bufferedOutputStream.write(new byte[]{(byte)0xff, (byte)0xff, (byte)0x01, (byte)0xfc, (byte)0xfc});
										bufferedOutputStream.flush();
										System.out.println("send cmd successed");
									} catch (IOException e) {
										e.printStackTrace();
									}
									Thread.sleep(2 * 60 * 60 * 1000);
								}
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}).start();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	/**发送有人进来指令**/
	private void sendSomeoneComeing(BufferedOutputStream bufferedOutputStream){
		try {
			bufferedOutputStream.write(Protocol.someoneComing);
			bufferedOutputStream.flush();
			System.out.println("发送有人进来指令");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
